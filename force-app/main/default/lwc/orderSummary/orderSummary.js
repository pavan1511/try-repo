import { LightningElement,api,wire,track } from 'lwc';
import getOrderSummary from '@salesforce/apex/B2BOrderSummaryController.getOrderSummary';
import getOrderShipments from '@salesforce/apex/B2BOrderSummaryController.getOrderShipments';
import getOrderItemSummary from '@salesforce/apex/ProductBundleController.getOrderItemSummary';
import getDeliveryMethod from '@salesforce/apex/ProductBundleController.getDeliveryMethod';
import CURRENCY from '@salesforce/i18n/currency';

import communityId from '@salesforce/community/Id';

export default class OrderSummary extends LightningElement {

    @api effectiveAccountId;
    @api recordId;
    orderSummary;
    orderShip;
    orderItem;
    orderProduct=[];
    deliveryMethod;
    order;
    shippingAdd = "Ship To: ";
  
   /* @wire(getOrderSummary, {
        communityId: communityId,
       
        effectiveAccountId: '$effectiveAccountId'
    })
    wiredOrderSummary({ error, data }) {
        if (data) {
           
            this.orderSummary = data;
            this.error = undefined;
           console.log('orderSummary:Data:'+this.orderSummary);
           
        } else if (error) {
            this.error = error;
            this.orderSummary = undefined;
           console.log('error:'+this.error);
        }
    };


    @wire(getOrderShipments, {
        communityId: communityId,
        orderSummaryId:'$recordId',
        effectiveAccountId: '$effectiveAccountId'
    })
    wiredOrderShippment({ error, data }) {
        if (data) {
           
            this.orderShip = data;
            this.error = undefined;
           console.log('orderShip:Data:'+this.orderShip);
           console.log('Record Id:'+this.recordId);
           
        } else if (error) {
            this.error = error;
            this.orderShip = undefined;
           console.log('error:'+this.error);
        }
    };
*/
    @wire(getOrderItemSummary, {
        orderSummaryId:'$recordId'
           })
    wiredOrderItemSummary({ error, data }) {
        if (data) {
            this.orderItem = data;
            this.error = undefined;
            //this.order = this.orderItem[0];
           
            
            this.currency=this.CURRENCY;
            for(let i=0 ; i<this.orderItem.length;i++){
                if(this.orderItem[i].ProductCode == null){
                    this.order = this.orderItem[i];
                }
                if(this.orderItem[i].TotalAmtWithTax!=0 && this.orderItem[i].ProductCode != null)
                this.orderProduct.push(this.orderItem[i]);
            }
            this.preTax=this.order.TotalPrice;
            console.log('orderProduct:Data:'+this.orderProduct);
            console.log('orderItem:Data:'+this.orderItem);
            console.log('order:Data:'+this.order);
        } else if (error) {
            this.error = error;
            this.orderItem = undefined;
           console.log('error:'+this.error);
        }
    };

    @wire(getDeliveryMethod, {
        orderSummaryId:'$recordId'
           })
    wiredDeliveryMethod({ error, data }) {
        if (data) {
           
            this.deliveryMethod = data;
            this.error = undefined;
           console.log('deliveryMethod:Data:'+this.deliveryMethod); 
           this.deliverymthd   = this.deliveryMethod.OrderDeliveryMethod.Name;
           this.shippingAdd = this.shippingAdd+ this.deliveryMethod.DeliverToAddress.street+", "+
           this.deliveryMethod.DeliverToAddress.city +", "+ this.deliveryMethod.DeliverToAddress.state+" "+  
           this.deliveryMethod.DeliverToAddress.postalCode +" "+this.deliveryMethod.DeliverToAddress.country; 
        } else if (error) {
            this.error = error;
            this.deliveryMethod = undefined;
           console.log('error:'+this.error);
        }
    };

    get isOrderEmpty() {
        // If the items are an empty array (not undefined or null), we know we're empty.
        return Array.isArray(this.orderProduct) && this.orderProduct.length === 0;
    }
   
    accordianSection = ['A'];

    handleToggleSection(event) {
          if(this.accordianSection.length > 0){
            this.accordianSection =''
        }
        else{
            this.accordianSection ='A'
        }

    }
 


}