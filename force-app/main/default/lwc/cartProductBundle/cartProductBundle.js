import { LightningElement, api, wire ,track} from 'lwc';
import getBundleItems from '@salesforce/apex/ProductBundleController.getBundleItems';


export default class CartProductBundle extends LightningElement {

    

    @api parentProduct='';
        productBundles;
        error;
    @api effectiveAcc;
   

    @wire(getBundleItems, {parentProductId : '$parentProduct'})
    //productBundle;
    wiredproductBundles({ error, data }) {
        if (data) {
            this.productBundles = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.productBundles = undefined;
        }
    }
   
        
    get compHeader(){
        console.log("ParentProduct:"+this.parentProduct);
       /* console.log("Bundle:"+this.data);
        console.log('2:effectAcc:'+this.effectiveAcc);*/
        return 'Items Included in this Bundle';
    }

    get isbundleEmpty() {
        // If the items are an empty array (not undefined or null), we know we're empty.
        return Array.isArray(this.productBundles) && this.productBundles.length === 0;
    }

    

}