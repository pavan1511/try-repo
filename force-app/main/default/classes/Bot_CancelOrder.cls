public with sharing class Bot_CancelOrder {

  public class OrderOutput {
    @InvocableVariable(required=true)
    public String sOrderStatus;
  }

  public class OrderInput {
    @InvocableVariable(required=true)
    public String sOrderNumber;
  }

  @InvocableMethod(label='Bot Cancel Order')
  public static List<OrderOutput> getOrderStatus(List<OrderInput> orderInputs) {
    Set<String> orderNumbers = new Set<String>(); 

    // Get the order numbers from the input
    for (OrderInput orderInput : orderInputs) {
      orderNumbers.add(orderInput.sOrderNumber);
    }

    // Get the order objects from the set of order numbers
    List<OrderSummary> order = 
      [SELECT id,OrderNumber,Status FROM OrderSummary WHERE OrderNumber in :orderNumbers];
    
     List<OrderSummary> updated = new List<OrderSummary>();
    if(order.size() > 0) {
        for (OrderSummary o : order){
            o.Status='Cancel';
            updated.add(o);
        }
      }
      update updated;
    List<OrderSummary> orders = 
      [SELECT id,OrderNumber,Status FROM OrderSummary WHERE OrderNumber in :orderNumbers]; 
    // Create a map of order numbers and order status values
    Map<String, String> mapNameStatus = new Map<String, String>(); 
    if (orders.size() > 0) {
        for (OrderSummary ord : orders) {
          mapNameStatus.put(ord.OrderNumber, ord.Status);
        }
    }
    
    // Build a list of order status values for the output
    List<OrderOutput> orderOutputs = new List<OrderOutput>();
    for (OrderInput orderInput : orderInputs) {
      OrderOutput orderOutput = new OrderOutput();
      
      // Do we have a status for this order number?
      if (mapNameStatus.containsKey(orderInput.sOrderNumber)) {
          // If so, then add the status
          orderOutput.sOrderStatus = mapNameStatus.get(orderInput.sOrderNumber);
      } else {
          // If not, then add an unknown status value
          orderOutput.sOrderStatus = 'Order not found';
      }
      orderOutputs.add(orderOutput);
    }

    return orderOutputs;    
  }
}