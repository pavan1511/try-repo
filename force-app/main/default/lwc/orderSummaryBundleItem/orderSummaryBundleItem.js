import { LightningElement, api, wire ,track} from 'lwc';
import getProduct from '@salesforce/apex/B2BGetInfo.getProduct';
import getProductPrice from '@salesforce/apex/B2BGetInfo.getProductPrice';
import communityId from '@salesforce/community/Id';
import { resolve } from 'c/cmsResourceResolver';
import { NavigationMixin } from 'lightning/navigation';


export default class OrderSummaryBundleItem extends NavigationMixin(LightningElement) {
   @api childProductId;
   @api quantity;
   @api resolvedEffectiveAccountId;
   productItem;
   name;
   image;
   sku;
   productPrice;
   price;
   producturl;

   
  
     /**
     * The full product information retrieved.
     *
     * @type {ConnectApi.ProductDetail}
     * @private
     */
      @wire(getProduct, {
        communityId: communityId,
        productId:'$childProductId',
        effectiveAccountId: '$resolvedEffectiveAccountId'
    })
    
   wiredproduct({ error, data }) {
        if (data) {
           
            this.productItem = data;
            this.error = undefined;
            
            console.log('productItem:Data:'+this.productItem);
            console.log('productItem:Data:fields.Name'+this.productItem.fields.Name);
            console.log('productItem:Data:fields.image'+resolve(this.productItem.defaultImage.url));
            this.url=window.location.href;
            this.arr = this.url.split("/");
            //this.producturl=this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/'+this.arr[4]+'/product/';
            //this.producturl = this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/';
            if(this.arr[4]=='s'){
                this.producturl=this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/'+this.arr[4]+'/product/';
            }
            else {
                this.producturl = this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/product/'; 
            }
            console.log('url:'+this.url);
            console.log('childProductId:'+this.productItem.id);
            this.producturl=this.producturl+this.productItem.id;
            console.log('producturl:'+this.producturl);
            this.name=this.productItem.fields.Name;
            this.image=resolve(this.productItem.defaultImage.url);
            this.altText=this.productItem.defaultImage.alternativeText;
            this.sku=this.productItem.fields.StockKeepingUnit;
            this.productCode=this.productItem.fields.ProductCode;
            this.imagelisting=this.productItem.mediaGroups;
            for(let i=0 ; i<this.imagelisting.length;i++){
                if(this.imagelisting[i].developerName == 'productListImage')
                this.imglist=resolve(this.imagelisting[i].mediaItems[0].url);
               
            }
            
        } else if (error) {
            this.error = error;
            this.productItem = undefined;
            console.log('error:'+this.error);
        }
    };

    @wire(getProductPrice, {
        communityId: communityId,
        productId: '$childProductId',
        effectiveAccountId: '$resolvedEffectiveAccountId'
    })
    wiredproductPrice({ error, data }) {
        if (data) {
           
            this.productPrice = data;
            this.error = undefined;
        /*    console.log('productPrice:Data:'+this.productPrice);*/
            this.price=this.productPrice.unitPrice;
            this.currency=this.productPrice.currencyIsoCode;
        } else if (error) {
            this.error = error;
            this.productPrice = undefined;
           /* console.log('error:'+this.error);*/
        }
    };

    /**
     * Handler for the 'click' event fired from 'contents'
     *
     * @param {Object} evt the event object
     */
     handleProductDetailNavigation(evt) {
        evt.preventDefault();
        const productId = evt.target.dataset.productid;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: productId,
                actionName: 'view'
            }
        });
    }
   
   

  
}