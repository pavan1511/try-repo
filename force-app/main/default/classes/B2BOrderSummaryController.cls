public with sharing class B2BOrderSummaryController {
    
    @AuraEnabled(cacheable=true)
    public static ConnectApi.OrderSummaryCollectionRepresentation getOrderSummary(
      String communityId,
      String effectiveAccountId ) {
      // Lookup the webstore ID associated with the community
      String webstoreId = B2BUtils.resolveCommunityIdToWebstoreId(communityId);
  
      
      return ConnectApi.CommerceBuyerExperience.getOrderSummaries( webstoreId,  effectiveAccountId);
    }

    @AuraEnabled(cacheable=true)
    public static ConnectApi.OrderShipmentCollection getOrderShipments(
      String communityId,
      String orderSummaryId,
      String effectiveAccountId ) {
      // Lookup the webstore ID associated with the community
      String webstoreId = B2BUtils.resolveCommunityIdToWebstoreId(communityId);
  
      
      return ConnectApi.CommerceBuyerExperience.getOrderShipments( webstoreId, orderSummaryId, effectiveAccountId);
    }
}