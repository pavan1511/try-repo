import { LightningElement,wire } from 'lwc';
import getRecentItems from '@salesforce/apex/ProductBundleController.getRecentItems';

export default class RecentProduct extends LightningElement {

  
  recentProduct;
    @wire(getRecentItems)
    wiredRecentProduct({ error, data }) {
        if (data) {
           
            this.recentProduct = data;
            this.error = undefined;
           console.log('recentProduct:Data:'+this.recentProduct);
           
        } else if (error) {
            this.error = error;
            this.recentProduct = undefined;
           console.log('error:'+this.error);
        }
    };


}