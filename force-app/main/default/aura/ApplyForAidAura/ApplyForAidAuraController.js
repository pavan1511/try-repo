({
	doInit : function(component, event, helper) {
        
        helper.logOutput(component, component.get("v.recordId"));
	},
    
    applyForAid: function(component, event, helper) {
    
        // Create the action
        var action = component.get("c.applyForAidAction");
        
        var recordId = component.get("v.recordId");

        if(recordId===null) 
            return;
        
        action.setParams({
            "studentID": recordId
        });
        
         // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                $A.get('e.force:refreshView').fire(); 
                helper.logOutput(component, component.get("v.recordId"));
            }
            else {
                helper.logOutput("Failed with state: " + state);
            }
        });
    
        // Send action off to be executed
        $A.enqueueAction(action);
    }
})