import { LightningElement, api } from 'lwc';

export default class ShowAccount extends LightningElement {
    @api strRecordId;
    @api TotalAmount;
    
    arrayFields = ['TotalAmount'];

    localvariable(){
        var team = [{lead:'Monica',projectManager:'Ross',intern:'Chandler'},
                 {lead:'Joey',projectManager:'Rachel',intern:'Phoebe'}];
         
        //Store it in the localstorage and name it localStr.
        localStorage.setItem('localStr',JSON.stringify(team));
         
        //Retrieve the data and parse it back to JSON.
        var retrieveData = JSON.parse(localStorage.getItem('localStr'));
         
        //Write the data to the console.
        console.log('and the lead of team 1 is '+ retrieveData[0].lead);
    }
}