public class HelloWorldClass {

    public void HelloWorld()
    {
        try {
            system.debug('Hello World');
        }
        catch(Exception ex)
        {
            // We'll never actually get here because the debug statement can't fail
            system.debug('Impossible error');
        }
    }
}