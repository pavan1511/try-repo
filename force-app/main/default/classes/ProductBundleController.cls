public with sharing class ProductBundleController {
    public ProductBundleController() {
    }
        @AuraEnabled(cacheable = true)
        public static List<lb2bt__Bundle_Item__c> getBundleItems(String parentProductId) {
            return [SELECT Id,lb2bt__Name__c,lb2bt__Quantity__c,lb2bt__Bundle_Item__c from lb2bt__Bundle_Item__c where lb2bt__Bundle_Parent_Item__c= :parentProductId ];
        }

        @AuraEnabled(cacheable = true)
        public static List<RecentlyViewed> getRecentItems() {
            return [SELECT Id, Name ,Type , LastViewedDate
            FROM RecentlyViewed
            WHERE LastViewedDate !=null and Type IN ('Product2')
            ORDER BY LastViewedDate DESC];
        }

        @AuraEnabled(cacheable = true)
        public static List<OrderItemSummary> getOrderItemSummary(String orderSummaryId) {
            return [SELECT ProductCode,StockKeepingUnit,	Product2Id,	TotalTaxAmount,TotalAmtWithTax,TotalPrice,Quantity,OriginalOrderItemId
            FROM OrderItemSummary  where OrderSummaryId = :orderSummaryId ORDER BY Id ];
        }
        
        @AuraEnabled(cacheable = true)
        public static OrderDeliveryGroupSummary getDeliveryMethod(String orderSummaryId) {
            return [SELECT  OrderDeliveryMethod.NAME ,DeliverToAddress
            FROM OrderDeliveryGroupSummary  where OrderSummaryId =:orderSummaryId   
             ];
        }

       




    
}