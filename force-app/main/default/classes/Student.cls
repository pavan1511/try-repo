public class Student {
    public String firstName;
    public String lastName;
    public Integer age;
    public double GPA;
    public Boolean appliedForFinancialAid;
    
    public String fullName()
    {
        return firstName + ' ' + lastName;
    }
}