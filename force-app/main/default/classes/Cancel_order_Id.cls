public with sharing class Cancel_order_Id {
    public Cancel_order_Id() {

    }

    @InvocableMethod(label='Cancel Order' description='Canceled the order.' category='B2B Commerce')
    public static void cancelOrder(List<String> orderIds) {

        String id = '';

        try {

            for(String orderId: orderIds){
                id = orderId;
            }
        
            OrderSummary os = Database.query(
              'SELECT Id, Status FROM Order WHERE id=\''+id+'\' LIMIT 1');
            os.status = 'Cancelled';
            update os;

        } catch (DmlException e) {
            String errorMessage = e.getMessage();
            System.debug('Error Message: ' + e);
        }
    }
}