import { LightningElement, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import APPLIEDFIELD from '@salesforce/schema/Student__c.AppliedForFinancialAid__c';
import { refreshApex } from '@salesforce/apex';
import applyForAidAction from '@salesforce/apex/StudentAidController.applyForAidAction';
import {logOutput} from 'c/logHelper';


export default class ApplyForAidLWC extends LightningElement {
    @api recordId;

    @wire(getRecord, { recordId: '$recordId', fields: [APPLIEDFIELD]})
    student;

    

    get appliedForFinancialAid() {
        return (this.student.data===undefined)? null: this.student.data.fields.AppliedForFinancialAid__c.value;
    }

    get appliedForFinancialAidStatus()
    {
        let applied = this.appliedForFinancialAid;
        if(applied==null) return 'Data not available ';
        return (applied)? 'Applied ':'No application on file ';
    }

    handleApplyForAid() {
        applyForAidAction({'studentID': this.recordId})
            .then(result => { 
                refreshApex(this.student);
                logOutput('Applied succesfully')
                logOutput(result);
                })
            .catch(error=> { 
                logOutput('Application failed ');
                logOutput(error);
            });
    }

}