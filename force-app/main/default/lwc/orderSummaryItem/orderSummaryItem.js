import { LightningElement, api, wire ,track} from 'lwc';
import getProduct from '@salesforce/apex/B2BGetInfo.getProduct';
import communityId from '@salesforce/community/Id';
import { resolve } from 'c/cmsResourceResolver';
import { NavigationMixin } from 'lightning/navigation';
import CURRENCY from '@salesforce/i18n/currency';
import getBundleItems from '@salesforce/apex/ProductBundleController.getBundleItems';

export default class CartProductBundleItem extends NavigationMixin(LightningElement) {
    
    @api product;
    @api quantity;
    @api effectiveaccountid;
    @api amt;
    producturl;
    name;
    sku;
    currency;
    productCode;
    productBundles;
 
    
   
      /**
      * The full product information retrieved.
      *
      * @type {ConnectApi.ProductDetail}
      * @private
      */
       @wire(getProduct, {
         communityId: communityId,
         productId:'$product',
         effectiveAccountId: '$effectiveaccountid'
     })
     
    wiredproduct({ error, data }) {
         if (data) {
            
             this.productItem = data;
             this.error = undefined;
             
             
             this.url=window.location.href;
             this.arr = this.url.split("/");
             //this.producturl=this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/'+this.arr[4]+'/product/';
             if(this.arr[4]=='s'){
                this.producturl=this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/'+this.arr[4]+'/product/';
            }
            else {
                this.producturl = this.arr[0]+'//'+this.arr[2]+'/'+this.arr[3]+'/product/'; 
            }
             this.producturl=this.producturl+this.productItem.id;
             
             this.name=this.productItem.fields.Name;
             this.image=resolve(this.productItem.defaultImage.url);
             this.altText=this.productItem.defaultImage.alternativeText;
             this.sku=this.productItem.fields.StockKeepingUnit;
             this.currency=this.productItem.fields.CurrencyIsoCode;
             this.productCode=this.productItem.fields.ProductCode;
             this.imagelisting=this.productItem.mediaGroups;
             
             
             for(let i=0 ; i<this.imagelisting.length;i++){
                if(this.imagelisting[i].developerName == 'productListImage')
                this.imglist=resolve(this.imagelisting[i].mediaItems[0].url);
               
            }
            
         } else if (error) {
             this.error = error;
             this.productItem = undefined;
             console.log('error:'+this.error);
         }
     };
 
    
     /**
      * Handler for the 'click' event fired from 'contents'
      *
      * @param {Object} evt the event object
      */
      handleProductDetailNavigation(evt) {
         evt.preventDefault();
         const productId = evt.target.dataset.productid;
         this[NavigationMixin.Navigate]({
             type: 'standard__recordPage',
             attributes: {
                 recordId: productId,
                 actionName: 'view'
             }
         });
     }
    
     @wire(getBundleItems, {parentProductId : '$product'})
     
     wiredproductBundles({ error, data }) {
         if (data) {
             this.productBundles = data;
             this.error = undefined;
         } else if (error) {
             this.error = error;
             this.productBundles = undefined;
         }
     }
    
         
     get compHeader(){
         console.log("ParentProduct:"+this.product);
        /* console.log("Bundle:"+this.data);
         console.log('2:effectAcc:'+this.effectiveAcc);*/
         return 'Items Included in this Bundle';
     }
 
     get isbundleEmpty() {
         // If the items are an empty array (not undefined or null), we know we're empty.
         return Array.isArray(this.productBundles) && this.productBundles.length === 0;
     }
    
      accordianSection = ['A'];

    handleToggleSection(event) {
          if(this.accordianSection.length > 0){
            this.accordianSection =''
        }
        else{
            this.accordianSection ='A'
        }

    }
 
   
 }